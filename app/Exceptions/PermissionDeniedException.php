<?php

namespace App\Exceptions;

use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;

class PermissionDeniedException extends \Exception
{
    use ApiResponse;

    protected $message = 'Нету доступа';

    protected $keyword = '';

    protected $code = 403;

    public function render(): JsonResponse
    {
        return $this->errorResponse($this->code, $this->message);
    }

}
