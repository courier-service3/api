<?php

namespace App\Http\Controllers;

use App\Http\Requests\Common\ListWithPaginationRequest;
use App\Http\Requests\Regions\CreateRequest;
use App\Repositories\RegionRepository;
use App\Services\RegionService;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;

class RegionController
{
	use ApiResponse;

	public function __construct()
	{
		$this->repo    = new RegionRepository();
		$this->service = new RegionService($this->repo);
	}

	/**
	 * @OA\Post (
	 *     path="/api/v1/regions",
	 *     summary = "Добавления региона",
	 *     operationId="create regions",
	 *     security={ {"bearer": {} }},
	 *     tags={"Регионы"},
	 *     @OA\Parameter(
	 *         name="name",
	 *         in="query",
	 *         description="name",
	 *         required=true,
	 *         @OA\Schema(type="string")
	 *     ),
	 *     @OA\Parameter(
	 *         name="code",
	 *         in="query",
	 *         description="Код региона",
	 *         required=true,
	 *         @OA\Schema(type="string")
	 *     ),
	 *     @OA\Response(
	 *         response="201",
	 *         description="Returns data",
	 *         content={
	 *             @OA\MediaType(
	 *                 mediaType="application/json",
	 *             )
	 *         }
	 *     ),
	 * )
	 *
	 *
	 * @param CreateRequest $request
	 *
	 * @return JsonResponse
	 */

	public function create(CreateRequest $request): JsonResponse
	{
		return $this->successResponse(
			$this->service->create(
				$request->validated()
			)
		);
	}

	/**
	 * @OA\Get (
	 *     path="/api/v1/regions/",
	 *     summary = "Список регионов (с пагинцаей)",
	 *     operationId="regions.list",
	 *     tags={"Регионы"},
	 *     security={ {"bearer": {} }},
	 *     @OA\Parameter(
	 *         name="page",
	 *         in="query",
	 *         description="Page",
	 *         required=false,
	 *         @OA\Schema(type="integer")
	 *     ),
	 *     @OA\Response(
	 *         response="200",
	 *         description="Returns data",
	 *         content={
	 *             @OA\MediaType(
	 *                 mediaType="application/json",
	 *             )
	 *         }
	 *     ),
	 * )
	 *
	 * @param ListWithPaginationRequest $request
	 * @return JsonResponse
	 */

	public function list(ListWithPaginationRequest $request): JsonResponse
	{
		return $this->successResponse(
			$this->service->list()
		);
	}
}
