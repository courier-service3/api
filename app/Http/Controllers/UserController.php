<?php

namespace App\Http\Controllers;


use App\Http\Requests\Common\ListWithPaginationRequest;
use App\Http\Requests\User\RegisterRequest;
use App\Http\Requests\User\UpdateUserRoleRequest;
use App\Repositories\UserRepository;
use App\Services\UserService;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
	use ApiResponse;

	private UserService    $service;
	private UserRepository $repo;

	public function __construct()
	{
		$this->repo    = new UserRepository();
		$this->service = new UserService($this->repo);
	}

	/**
	 * @OA\Post (
	 *     path="/api/v1/users/",
	 *     summary = "Регистрация",
	 *     operationId="register",
	 *     tags={"Пользователи"},
	 *     @OA\Parameter(
	 *         name="email",
	 *         in="query",
	 *         description="email",
	 *         required=true,
	 *         @OA\Schema(type="string")
	 *     ),
	 *     @OA\Parameter(
	 *         name="name",
	 *         in="query",
	 *         description="name",
	 *         required=true,
	 *         @OA\Schema(type="string")
	 *     ),
	 *     @OA\Parameter(
	 *         name="region_id",
	 *         in="query",
	 *         description="region id",
	 *         required=true,
	 *         @OA\Schema(type="integer")
	 *     ),
	 *     @OA\Parameter(
	 *         name="password",
	 *         in="query",
	 *         description="password",
	 *         required=true,
	 *         @OA\Schema(format = "password")
	 *     ),
	 *     @OA\Response(
	 *         response="201",
	 *         description="Returns data",
	 *         content={
	 *             @OA\MediaType(
	 *                 mediaType="application/json",
	 *             )
	 *         }
	 *     ),
	 * )
	 *
	 *
	 * @param RegisterRequest $request
	 *
	 * @return JsonResponse
	 */

	public function register(RegisterRequest $request): JsonResponse
	{
		return $this->successResponse(
			$this->service->register(
				$request->validated()
			)
		);
	}

	/**
	 * @OA\Get (
	 *     path="/api/v1/users/",
	 *     summary = "Список пользователей (с пагинцаей)",
	 *     operationId="users.list",
	 *     tags={"Пользователи"},
	 *     security={ {"bearer": {} }},
	 *     @OA\Parameter(
	 *         name="page",
	 *         in="query",
	 *         description="Page",
	 *         required=false,
	 *         @OA\Schema(type="integer")
	 *     ),
	 *     @OA\Response(
	 *         response="200",
	 *         description="Returns data",
	 *         content={
	 *             @OA\MediaType(
	 *                 mediaType="application/json",
	 *             )
	 *         }
	 *     ),
	 * )
	 *
	 * @return JsonResponse
	 */

	public function list(ListWithPaginationRequest $request): JsonResponse
	{
		$page = $request->validated()['page'] ?? 1;

		return $this->successResponse(
			$this->service->list(
				$page
			)
		);
	}

	/**
	 * @OA\Put (
	 *     path="/api/v1/users/",
	 *     summary = "Назначение администраторов из таблицы пользователей",
	 *     operationId="update.user.role",
	 *     tags={"Пользователи"},
	 *     security={ {"bearer": {} }},
	 *     @OA\Parameter(
	 *         name="user_id",
	 *         in="query",
	 *         description="user id",
	 *         required=true,
	 *         @OA\Schema(type="integer")
	 *     ),
	 *     @OA\Parameter(
	 *         name="role_id",
	 *         in="query",
	 *         description="role id",
	 *         required=true,
	 *         @OA\Schema(type="integer")
	 *     ),
	 *     @OA\Response(
	 *         response="200",
	 *         description="Returns data",
	 *         content={
	 *             @OA\MediaType(
	 *                 mediaType="application/json",
	 *             )
	 *         }
	 *     ),
	 * )
	 *
	 * @return JsonResponse
	 */

	public function updateUserRole(UpdateUserRoleRequest $request): JsonResponse
	{
		$result = $this->service->updateUserRole(
			$request->validated()
		);

		if ($result) {
			return $this->successResponse(
				true,
				'Роль пользователя успешно обновился'
			);
		} else {
			return $this->errorResponse(
				409,
				'Роль пользователя не обновился, в связи ошибкой'
			);
		}
	}
}
