<?php

namespace App\Http\Controllers;

use App\Http\Requests\Application\CreateRequest;
use App\Http\Requests\Application\UpdateActiveStatus;
use App\Http\Requests\Common\ListWithPaginationRequest;
use App\Http\Requests\User\UpdateUserRoleRequest;
use App\Repositories\ApplicationRepository;
use App\Services\ApplicationService;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;

class ApplicationController
{
	use ApiResponse;

	private ApplicationRepository $repo;
	private ApplicationService    $service;

	public function __construct()
	{
		$this->repo    = new ApplicationRepository();
		$this->service = new ApplicationService($this->repo);
	}

	/**
	 * @OA\Post (
	 *     path="/api/v1/applications",
	 *     summary = "Создания заявки (Доступ только для ПОЛЬЗОВАТЕЛЕЙ)",
	 *     operationId="create.application",
	 *     security={ {"bearer": {} }},
	 *     tags={"Заявка"},
	 *     @OA\Parameter(
	 *         name="source",
	 *         in="query",
	 *         description="source addr",
	 *         required=true,
	 *         @OA\Schema(type="string")
	 *     ),
	 *     @OA\Parameter(
	 *         name="destination",
	 *         in="query",
	 *         description="destination addr",
	 *         required=true,
	 *         @OA\Schema(type="string")
	 *     ),
	 *     @OA\Parameter(
	 *         name="date",
	 *         in="query",
	 *         description="date, example : 11-11-2020 18-00",
	 *         required=true,
	 *         @OA\Schema(type="string")
	 *     ),
	 *     @OA\Response(
	 *         response="201",
	 *         description="Returns data",
	 *         content={
	 *             @OA\MediaType(
	 *                 mediaType="application/json",
	 *             )
	 *         }
	 *     ),
	 * )
	 *
	 *
	 * @param CreateRequest $request
	 *
	 * @return JsonResponse
	 */

	public function create(CreateRequest $request): JsonResponse
	{
		return $this->successResponse(
			$this->service->create(
				$request->validated()
			)
		);
	}

	/**
	 * @OA\Get (
	 *     path="/api/v1/applications/",
	 *     summary = "Список заявок (с пагинцаей)",
	 *     operationId="applications.list",
	 *     tags={"Заявка"},
	 *     security={ {"bearer": {} }},
	 *     @OA\Parameter(
	 *         name="page",
	 *         in="query",
	 *         description="Page",
	 *         required=false,
	 *         @OA\Schema(type="integer")
	 *     ),
	 *     @OA\Response(
	 *         response="200",
	 *         description="Returns data",
	 *         content={
	 *             @OA\MediaType(
	 *                 mediaType="application/json",
	 *             )
	 *         }
	 *     ),
	 * )
	 *
	 * @param ListWithPaginationRequest $request
	 * @return JsonResponse
	 */

	public function list(ListWithPaginationRequest $request): JsonResponse
	{
		return $this->successResponse(
			$this->service->list()
		);
	}

	/**
	 * @OA\Put (
	 *     path="/api/v1/applications",
	 *     summary = "Изменить статус заявки",
	 *     operationId="update.application.status",
	 *     tags={"Заявка"},
	 *     security={ {"bearer": {} }},
	 *     @OA\Parameter(
	 *         name="application_id",
	 *         in="query",
	 *         description="application id",
	 *         required=true,
	 *         @OA\Schema(type="integer")
	 *     ),
	 *     @OA\Parameter(
	 *         name="active",
	 *         in="query",
	 *         description="change active status",
	 *         required=true,
	 *         @OA\Schema(type="integer", enum={1,0})
	 *     ),
	 *     @OA\Response(
	 *         response="200",
	 *         description="Returns data",
	 *         content={
	 *             @OA\MediaType(
	 *                 mediaType="application/json",
	 *             )
	 *         }
	 *     ),
	 * )
	 *
	 * @return JsonResponse
	 */

	public function updateApplicationStatus(UpdateActiveStatus $request): JsonResponse
	{
		$result = $this->service->updateApplicationStatus(
			$request->validated()
		);

		if ($result) {
			return $this->successResponse(
				true,
				'Статус заявки успешно обновился'
			);
		} else {
			return $this->errorResponse(
				409,
				'Статус заявки не обновился, в связи ошибкой'
			);
		}
	}
}
