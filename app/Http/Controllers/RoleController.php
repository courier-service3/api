<?php

namespace App\Http\Controllers;

use App\Repositories\RolesRepository;
use App\Services\RolesService;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;

class RoleController
{
	use ApiResponse;

	private RolesRepository $repo;
	private RolesService    $service;

	public function __construct()
	{
		$this->repo    = new RolesRepository();
		$this->service = new RolesService($this->repo);
	}

	/**
	 * @OA\Get (
	 *     path="/api/v1/roles",
	 *     summary = "Список ролей",
	 *     operationId="roles.list",
	 *     tags={"Роли"},
	 *     security={ {"bearer": {} }},
	 *     @OA\Response(
	 *         response="200",
	 *         description="Returns data",
	 *         content={
	 *             @OA\MediaType(
	 *                 mediaType="application/json",
	 *             )
	 *         }
	 *     ),
	 * )
	 *
	 * @return JsonResponse
	 */

	public function list(): JsonResponse
	{
		return $this->successResponse(
			$this->service->getAll()
		);
	}
}
