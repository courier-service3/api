<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;

class AuthController
{
	use ApiResponse;

	/**
	 * @OA\Post (
	 *     path="/api/v1/login",
	 *     operationId="AuthLogin",
	 *     tags={"Авторизация"},
	 *     summary = "Авторизация",
	 *     @OA\Parameter(
	 *         name="email",
	 *         in="query",
	 *         description="email",
	 *         required=true,
	 *         @OA\Schema(type="string")
	 *     ),
	 *     @OA\Parameter(
	 *         name="password",
	 *         in="query",
	 *         description="password",
	 *         required=true,
	 *         @OA\Schema(format = "password")
	 *     ),
	 *     @OA\Response(
	 *         response="200",
	 *         description="Returns data",
	 *         content={
	 *             @OA\MediaType(
	 *                 mediaType="application/json",
	 *             )
	 *         }
	 *     ),
	 * )
	 *
	 * @param LoginRequest $request
	 *
	 * @return JsonResponse
	 */

	public function login(LoginRequest $request): JsonResponse
	{
		$token = auth()->attempt($request->validated());

		if (!$token) {
			return $this->errorResponse(
				403,
				'Unauthorized'
			);
		};

		return $this->successResponse(
			[
				'token'      => 'Bearer ' . $token,
				'token_type' => 'bearer',
				'expire_in'  => (int)(config('jwt.ttl') * 60)
			]
		);
	}
}
