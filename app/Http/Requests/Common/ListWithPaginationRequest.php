<?php

namespace App\Http\Requests\Common;

class ListWithPaginationRequest extends CommonRequest
{
    public function rules(): array
    {
        return [
            'page' => ['integer', 'gt:0']
        ];
    }
}
