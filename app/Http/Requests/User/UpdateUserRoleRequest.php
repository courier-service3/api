<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Common\CommonRequest;

class UpdateUserRoleRequest extends CommonRequest
{
    public function rules(): array
    {
        return [
            'user_id' => ['required', 'exists:users,id'],
            'role_id' => ['required', 'exists:roles,id'],
        ];
    }
}
