<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Common\CommonRequest;

class RegisterRequest extends CommonRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'regex:/^[a-zA-Z-w-яңәіқұүғөһё\s.\-]*$/'],
            'email' => ['required', 'string', 'email:rfc', 'max:255', 'unique:users'],
            'password' => ['required', 'min:8'],
            'region_id' => ['required','integer', 'exists:regions,id']
        ];
    }
}
