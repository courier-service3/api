<?php

namespace App\Http\Requests\Regions;

use App\Http\Requests\Common\CommonRequest;

class CreateRequest extends CommonRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'unique:regions,name'],
            'code' => ['required', 'string', 'unique:regions,code']
        ];
    }
}
