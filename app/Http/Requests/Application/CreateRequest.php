<?php

namespace App\Http\Requests\Application;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
	public function rules(): array
	{
		return [
			'source'      => ['required', 'string', 'max:255'],
			'destination' => ['required', 'string', 'max:255'],
			'date'        => ['required', 'date_format:d-m-Y H-i']
		];
	}
}
