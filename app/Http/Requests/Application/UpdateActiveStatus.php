<?php

namespace App\Http\Requests\Application;

use App\Http\Requests\Common\CommonRequest;

class UpdateActiveStatus extends CommonRequest
{
    public function rules(): array
    {
        return [
            'application_id' => ['required', 'exists:applications,id'],
            'active' => ['required', 'in:1,0'],
        ];
    }
}
