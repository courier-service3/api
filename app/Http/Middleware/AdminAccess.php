<?php

namespace App\Http\Middleware;

use App\Exceptions\PermissionDeniedException;
use App\Interfaces\Contracts\RoleInterface;
use App\Repositories\UserRoleRepository;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminAccess
{
	public function handle(Request $request, Closure $next): Response
	{
		$userRoleId = UserRoleRepository::getRoleByUserId(auth()->user()->id);

		if ($userRoleId == RoleInterface::ADMIN_ROLE_ID) {
			return $next($request);
		}

		throw new PermissionDeniedException();
	}
}
