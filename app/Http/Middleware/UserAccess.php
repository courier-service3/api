<?php

namespace App\Http\Middleware;

use App\Exceptions\PermissionDeniedException;
use App\Interfaces\Contracts\RoleInterface;
use App\Repositories\UserRoleRepository;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserAccess
{
	/**
	 * Handle an incoming request.
	 *
	 * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response) $next
	 */
	public function handle(Request $request, Closure $next): Response
	{
		$userRoleId = UserRoleRepository::getRoleByUserId(auth()->user()->id);

		if ($userRoleId == RoleInterface::USER_ROLE_ID) {
			return $next($request);
		}

		throw new PermissionDeniedException();
	}
}
