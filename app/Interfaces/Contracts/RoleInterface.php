<?php

namespace App\Interfaces\Contracts;

interface RoleInterface
{
    const USER_ROLE_ID = 2;
    const ADMIN_ROLE_ID = 1;
}
