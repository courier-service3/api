<?php

namespace App\Interfaces\Repository;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

interface RegionRepositoryInterface
{
    public function create(array $params): Model;
}
