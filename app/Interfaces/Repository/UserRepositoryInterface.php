<?php

namespace App\Interfaces\Repository;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

interface UserRepositoryInterface
{
    public function create(array $params): User|false;

    public function getAll(int $page): array;

    public function updateUserRole(array $params): int;
}
