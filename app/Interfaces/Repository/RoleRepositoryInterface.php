<?php

namespace App\Interfaces\Repository;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

interface RoleRepositoryInterface
{
    public function getAll(): array;
}
