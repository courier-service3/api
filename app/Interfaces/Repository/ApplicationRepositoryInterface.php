<?php

namespace App\Interfaces\Repository;

use Illuminate\Database\Eloquent\Model;

interface ApplicationRepositoryInterface
{
    public function create(array $params): Model;

	public function update(array $params): Model;

    public function getAll(): array;

    public function updateApplicationStatus(array $params): int;

	public function findExistingApplications(array $params): array;

	public function deletingExistingApplications(int $id);
}
