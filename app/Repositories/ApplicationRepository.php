<?php

namespace App\Repositories;

use App\Helpers\PaginationHelper;
use App\Interfaces\Repository\ApplicationRepositoryInterface;
use App\Models\Application;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class ApplicationRepository implements ApplicationRepositoryInterface
{
	public function create(array $params): Model
	{
		$userData = auth()->user();
		$date     = Arr::get($params, 'date', Carbon::now());

		$newApplication              = new Application();
		$newApplication->user_id     = $userData->id;
		$newApplication->region_id   = $userData->region_id;
		$newApplication->source      = $params['source'];
		$newApplication->destination = $params['destination'];
		$newApplication->date        = $date;
		$newApplication->save();

		return $newApplication;

	}

	public function getAll(): array
	{
		return PaginationHelper::handle(
			Application::query()
				->select([
					'applications.id',
					'source',
					'destination',
					'applications.created_at',
					'active',
					'users.name as user_name',
					'regions.name as region_name'
				])
				->leftJoin(
					'users',
					'applications.user_id',
					'=',
					'users.id'
				)
				->leftJoin(
					'regions',
					'applications.region_id',
					'=',
					'regions.id'
				)
				->paginate(
					PaginationHelper::DEFAULT_PAGINATION_PER_PAGE
				)
		);
	}

	public function updateApplicationStatus(array $params): int
	{
		return Application::query()
			->where('id', '=', $params['application_id'])
			->update(['active' => $params['active']]);
	}

	public function findExistingApplications(array $params): array
	{
		return Application::query()
			->select('*')
			->where('source', $params['source'])
			->where('destination', $params['destination'])
			->where('region_id', $params['region_id'])
			->where('date', $params['date'])
			->get()
			->toArray();
	}

	public function update(array $params): Model
	{
		$userData = auth()->user();

		$updatingApplication              = new Application();
		$updatingApplication->user_id     = $userData->id;
		$updatingApplication->region_id   = $userData->region_id;
		$updatingApplication->source      = $params['source'];
		$updatingApplication->destination = $params['destination'];
		$updatingApplication->date        = $params['date'];
		$updatingApplication->save();


		return $updatingApplication;

	}

	public function deletingExistingApplications(int $id)
	{
		return Application::query()
			->where('id', '=', $id)
			->delete();
	}

	public static function getApplicationById(int $id): Model
	{
		return Application::where('id', '=', $id)->first();
	}
}
