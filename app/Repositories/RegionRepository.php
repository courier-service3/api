<?php

namespace App\Repositories;

use App\Helpers\PaginationHelper;
use App\Interfaces\Repository\RegionRepositoryInterface;
use App\Interfaces\Repository\RoleRepositoryInterface;
use App\Interfaces\Repository\UserRepositoryInterface;
use App\Models\Region;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class RegionRepository implements RegionRepositoryInterface
{
	public function create(array $params): Model
	{
		$region       = new Region;
		$region->name = $params['name'];
		$region->code = $params['code'];
		$region->saveOrFail();

		return $region;
	}

	public function getAll(): array
	{
		return PaginationHelper::handle(
			Region::query()
				->paginate(PaginationHelper::DEFAULT_PAGINATION_PER_PAGE)
		);
	}
}
