<?php

namespace App\Repositories;

use App\Interfaces\Contracts\RoleInterface;
use App\Models\UserRole;
use Illuminate\Database\Eloquent\Model;

class UserRoleRepository
{
	public static function create(int $userId, ?int $roleId = null): Model
	{
		return UserRole::query()->create([
			'user_id' => $userId,
			'role_id' => $roleId ?? RoleInterface::USER_ROLE_ID,
		]);
	}

	public static function getRoleByUserId(int $userId): int
	{
		return UserRole::query()
			->where('user_id', '=', $userId)
			->pluck('role_id')
			->first();
	}
}
