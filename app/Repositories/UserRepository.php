<?php

namespace App\Repositories;

use App\Helpers\PaginationHelper;
use App\Interfaces\Repository\UserRepositoryInterface;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserRepository implements UserRepositoryInterface
{
	public function create(array $params): User|false
	{
		DB::beginTransaction();

		try {
			$user            = new User;
			$user->name      = $params['name'];
			$user->password  = Hash::make($params['password']);
			$user->email     = $params['email'];
			$user->region_id = $params['region_id'];
			$user->saveOrFail();

			UserRoleRepository::create($user->id);

			DB::commit();

			return $user;
		} catch (\Exception $exception) {
			Log::error(
				sprintf('Error while creating user, user_id - %d', $user->id),
				[
					'Location' => $exception->getFile(),
					'Line'     => $exception->getLine()
				]
			);

			DB::rollBack();

			return false;
		}
	}

	public function getAll(int $page): array
	{
		return PaginationHelper::handle(
			User::query()
				->select([
					'users.id as id',
					'users.name as name',
					'roles.name as role'
				])
				->leftJoin(
					'user_roles',
					'users.id',
					'=',
					'user_roles.user_id'
				)
				->leftJoin(
					'roles',
					'user_roles.role_id',
					'=',
					'roles.id'
				)->paginate(PaginationHelper::DEFAULT_PAGINATION_PER_PAGE)
		);
	}

	public function updateUserRole(array $params): int
	{
		return UserRole::query()
			->where('user_id', '=', $params['user_id'])
			->update(['role_id' => $params['role_id']]);
	}
}
