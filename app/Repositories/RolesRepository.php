<?php

namespace App\Repositories;

use App\Helpers\PaginationHelper;
use App\Interfaces\Repository\RoleRepositoryInterface;
use App\Interfaces\Repository\UserRepositoryInterface;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class RolesRepository implements RoleRepositoryInterface
{
	public function getAll(): array
	{
		return Role::query()->get()->toArray();
	}
}
