<?php

namespace App\Services;

use App\Repositories\RolesRepository;

class RolesService
{
    public function __construct(RolesRepository $rolesRepository)
    {
        $this->repo = $rolesRepository;
    }

    public function getAll(): array
    {
        return $this->repo->getAll();
    }
}
