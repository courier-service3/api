<?php

namespace App\Services;

use App\Interfaces\Repository\RegionRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class RegionService
{
    public function __construct(RegionRepositoryInterface $regionRepository)
    {
        $this->repo = $regionRepository;
    }

    public function create(array $params): Model
    {
        return $this->repo->create($params);
    }

    public function list(): array
    {
        return $this->repo->getAll();
    }
}
