<?php

namespace App\Services;

use App\Interfaces\Repository\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class UserService
{
    private UserRepositoryInterface $repo;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->repo = $userRepository;
    }

    public function register(array $params): Model|false
    {
        return $this->repo->create($params);
    }

    public function list(int $page): array
    {
        return $this->repo->getAll($page);
    }

    public function updateUserRole(array $params): int
    {
        return $this->repo->updateUserRole($params);
    }
}
