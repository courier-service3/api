<?php

namespace App\Services;

use App\Interfaces\Repository\ApplicationRepositoryInterface;
use App\Repositories\ApplicationRepository;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ApplicationService
{
	private ApplicationRepositoryInterface $repo;

	public function __construct(ApplicationRepositoryInterface $applicationRepository)
	{
		$this->repo = $applicationRepository;
	}

	public function create(array $params): Model
	{
		$date                = Carbon::createFromFormat('d-m-Y H-i', $params['date']);
		$params['date']      = $date->format('Y-m-d');
		$params['region_id'] = auth()->user()->region_id;

		$existingApplications = $this->findMatchingApplicationData($params);

		if (!empty($existingApplications)) {
			$combinedData       = $this->combineApplicationData($existingApplications);
			$updatedApplication = $this->repo->update($combinedData);
			$this->deletingExistingApplications($existingApplications);

			return ApplicationRepository::getApplicationById($updatedApplication->id);
		} else {
			return $this->repo->create($params);
		}
	}

	public function list(): array
	{
		return $this->repo->getAll();
	}

	public function updateApplicationStatus(array $params): int
	{
		return $this->repo->updateApplicationStatus($params);
	}

	private function findMatchingApplicationData(array $params): array
	{
		return $this->repo->findExistingApplications($params);
	}

	private function combineApplicationData(array $params): array
	{
		$combinedData = [];

		foreach ($params as $eachParam) {
			$date                        = Carbon::createFromFormat('d-m-Y', $eachParam['date']);
			$eachParam['date']           = $date->format('Y-m-d');
			$combinedData['source']      = $eachParam['source'];
			$combinedData['destination'] = $eachParam['destination'];
			$combinedData['date']        = $eachParam['date'];
			$combinedData['region_id']   = $eachParam['region_id'];

		};

		return $combinedData;
	}

	private function deletingExistingApplications(array $params): void
	{
		foreach ($params as $eachParam) {

			$this->repo->deletingExistingApplications($eachParam['id']);
		}
	}
}
