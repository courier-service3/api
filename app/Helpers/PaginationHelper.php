<?php

namespace App\Helpers;

class PaginationHelper
{
    public const DEFAULT_PAGINATION_PER_PAGE = 10;

    public static function handle($result, $nullable = false): ?array
    {
        if ($nullable && $result->total() == 0) {
            return null;
        }
        $data                 = null;
        $data['total']        = $result->total();
        $data['current_page'] = $result->currentPage();
        $data['last_page']    = $result->lastPage();
        $data['details']      = $result->items();

        return $data;
    }

}
