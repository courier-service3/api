<?php

namespace Database\Seeders;

use App\Models\Region;
use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
	public function run(): void
	{
		$items = [
			[
				'id'   => 1,
				'name' => 'Астана',
				'code' => 'ast'
			],
			[
				'id'   => 2,
				'name' => 'Алматы',
				'code' => 'ata'
			],
		];

		foreach ($items as $item) {
			Region::query()
				->firstOrCreate(
					[
						'id'   => $item['id'],
						'name' => $item['name'],
					],
					$item);
		}
	}
}
