<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
	public function run(): void
	{
		$items = [
			[
				'id'        => 1,
				'name'      => 'Admin',
				'email'     => 'admin@admin.kz',
				'password'  => Hash::make('Admin123'),
				'region_id' => 99999999
			],
			[
				'id'        => 2,
				'name'      => 'Пользователь',
				'email'     => 'user@user.kz',
				'password'  => Hash::make('Admin123'),
				'region_id' => 1
			],
		];

		foreach ($items as $item) {
			User::query()
				->firstOrCreate(
					[
						'id'    => $item['id'],
						'name'  => $item['name'],
						'email' => $item['email'],
					],
					$item);
		}

//        User::query()
//            ->selectRaw("setval(pg_get_serial_sequence('users', 'id'), max(id))")
//            ->get();
	}
}
