<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
	public function run(): void
	{
		$items = [
			[
				'id'   => 1,
				'name' => 'Админ',
				'code' => 'admin'
			],
			[
				'id'   => 2,
				'name' => 'Пользователь',
				'code' => 'user'
			],
		];

		foreach ($items as $item) {
			Role::query()
				->firstOrCreate(
					[
						'id'   => $item['id'],
						'name' => $item['name'],
						'code' => $item['code'],
					],
					$item);
		}
	}
}
