<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\UserRole;
use Illuminate\Database\Seeder;

class UserRolesTableSeeder extends Seeder
{
	public function run(): void
	{
		$items = [
			[
				'id'      => 1,
				'user_id' => 1,
				'role_id' => 1
			],
			[
				'id'      => 2,
				'user_id' => 2,
				'role_id' => 2
			]
		];

		foreach ($items as $item) {
			UserRole::query()
				->firstOrCreate(
					[
						'id'      => $item['id'],
						'user_id' => $item['user_id'],
						'role_id' => $item['role_id'],
					],
					$item);
		}
	}
}
