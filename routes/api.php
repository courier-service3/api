<?php

use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\RegionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['middleware' => 'api', 'prefix' => 'v1'], function () {
	Route::post('users', [UserController::class, 'register']);
	Route::post('login', [AuthController::class, 'login']);

	Route::group(['middleware' => 'auth'], function () {
		Route::get('regions', [RegionController::class, 'list']);
		Route::group(['middleware' => 'admin'], function () {
			Route::group(['prefix' => 'users'], function () {
				Route::get('', [UserController::class, 'list']);
				Route::put('', [UserController::class, 'updateUserRole']);
			});

			Route::get('roles', [RoleController::class, 'list']);
			Route::post('regions', [RegionController::class, 'create']);
			Route::group(['prefix' => 'applications'], function () {
				Route::get('', [ApplicationController::class, 'list']);
				Route::put('', [ApplicationController::class, 'updateApplicationStatus']);
			});
		});
		
		Route::group(['middleware' => 'user'], function () {
			Route::post('applications', [ApplicationController::class, 'create']);
		});
	});
});
