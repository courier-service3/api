# Проект "Вызов Курьера"

Привет! Добро пожаловать в проект "Вызов Курьера". Это руководство поможет вам начать работу и выполнить основные этапы проекта.


## Установка и настройка

1. Необходимо спулить Docker окружение (main ветка):
   ```shell
   git clone https://gitlab.com/courier-service3/docker

2. Необходимо спулить Api (main ветка):
   ```shell
   git clone https://gitlab.com/courier-service3/api

3. В Докере .env поставить свои настройки.

4. Выполнить команду для сборки Docker контейнеров:

   ```shell
   docker compose build

5. Запустить проект:
   ```shell
   docker compose up -d

6. Выполнить команду для установки и настройки Courier Call API:
    ```shell
   sh installation/installation/install_courier_call_api.sh

7. Перейти по ссылке http://localhost:<ваш_порт>/api/documentation для доступа к документации Swagger API.

## Описание проекта

В данный момент Web интерфейс не успел сделать, но для какого-то удобство написал документации Swagger API.

## Начальные данные

Дефолтный админ : Логин - admin@admin.kz, Пароль - Admin123.

Дефолтный пользователь : Логин - user@user.kz, Пароль - Admin123