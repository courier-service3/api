<?php

namespace Tests\Feature;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class UserController extends TestCase
{
	/**
	 * @covers       \App\Http\Controllers\UserController::register
	 *
	 * @dataProvider getDataForRegisterTest
	 *
	 * @param array $data
	 * @param array $resultStructure
	 * @param int $statusCode
	 *
	 * @return void
	 */
	public function test_register(array $data, array $resultStructure, int $statusCode): void
	{
		$response = $this->json(Request::METHOD_POST, "/api/v1/users", $data);
		$response->assertStatus($statusCode);
		$response->assertJsonStructure($resultStructure);
	}

	public static function getDataForRegisterTest(): array
	{
		return [
			[
				'data'             => [
					'email'    => 'admin2@admin.kz',
					'password' => 'Admin123',
					'region_id' => 1,
					'name' => 'Admin Admin'
				],
				'result_structure' => [
					'status',
					'data' => [
						'name',
						'email',
						'updated_at',
						'created_at',
						'id',
						'region_id'
					],
					'message',
				],
				'status_code'      => Response::HTTP_OK
			],
			[
				'data'             => [
					'email'    => 1,
					'password' => 1,
					'region_id' => 'region'
				],
				'result_structure' => [
					'message',
					'errors' => [
						'email',
						'password',
						'region_id'
					],
				],
				'status_code'      => Response::HTTP_UNPROCESSABLE_ENTITY
			]
		];
	}

	/**
	 * @covers       \App\Http\Controllers\UserController::list
	 *
	 * @dataProvider getDataForListTest
	 *
	 * @param array $data
	 * @param array $resultStructure
	 * @param int $statusCode
	 *
	 * @return void
	 */
	public function test_list(array $data, array $resultStructure, int $statusCode): void
	{
		$this->withoutMiddleware();
		$response = $this->json(Request::METHOD_GET, "/api/v1/users", $data);
		$response->assertStatus($statusCode);
		$response->assertJsonStructure($resultStructure);
	}

	public static function getDataForListTest(): array
	{
		return [
			[
				'data' => [
					'page' => 2
				],
				'result_structure' => [
					'status',
					'data' => [
						'total',
						'current_page',
						'last_page',
						'details',
					],
					'message',
				],
				'status_code'      => Response::HTTP_OK
			],
			[
				'data'             => [
					'page' => 'region'
				],
				'result_structure' => [
					'message',
					'errors' => [
						'page'
					],
				],
				'status_code'      => Response::HTTP_UNPROCESSABLE_ENTITY
			]
		];
	}

}
