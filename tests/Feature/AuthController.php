<?php

namespace Tests\Feature;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class AuthController extends TestCase
{
	/**
	 * @covers       \App\Http\Controllers\AuthController::login
	 *
	 * @dataProvider getDataForLoginTest
	 *
	 * @param array $data
	 * @param array $resultStructure
	 * @param int $statusCode
	 *
	 * @return void
	 */
	public function test_login(array $data, array $resultStructure, int $statusCode): void
	{
		$response = $this->json(Request::METHOD_POST, "/api/v1/login", $data);
		$response->assertStatus($statusCode);
		$response->assertJsonStructure($resultStructure);
	}

	public static function getDataForLoginTest(): array
	{
		return [
			[
				'data'             => [
					'email'    => 'admin@admin.kz',
					'password' => 'Admin123'
				],
				'result_structure' => [
					'status',
					'data' => [
						'token',
						'token_type',
						'expire_in'
					],
					'message',
				],
				'status_code'      => Response::HTTP_OK
			],
			[
				'data'             => [
					'email'    => 1,
					'password' => 1
				],
				'result_structure' => [
					'message',
					'errors' => [
						'email',
						'password'
					],
				],
				'status_code'      => Response::HTTP_UNPROCESSABLE_ENTITY
			]
		];
	}


}
